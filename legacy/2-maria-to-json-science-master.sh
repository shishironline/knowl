#!/bin/bash
##############################################################
## Knowl Bookshelf - INGEST Science                         ##
## Filename: ./legacy/2-maria-to-json-science.sh            ##
## https://gitlab.com/lucidhack/knowl                       ##
## https://creativecommons.org/licenses/by/4.0/             ##
## Author(s): lucidhack                                     ##
##############################################################

# PURGE EXISTING FILES
cd /var/tmp
sudo rm -f ~/knowl/ingest/knowl-libgen-science-master-v1_1.json
sudo rm -f ~/knowl/ingest/knowl-libgen-science-master_temp.json
sudo rm -f /var/tmp/knowl-libgen-science-master*.json

# GENERATE MANY JSON FILES (50K at a time)
counter=0
while [ $counter -le 2500000 ]
do
echo $counter
sudo mysql <<ENDSQL
use libgen;
DROP TABLE IF EXISTS knowl;
SET connect_work_size = 4000000000;
create table knowl (TOC char(255), Description char(255)) engine=CONNECT table_type=JSON file_name='/var/tmp/knowl-libgen-science-master-$counter.json' option_list='Pretty=1,Jmode=0,Base=1' lrecl=128 as
SELECT updated.ID,
       updated.Title,
       updated.VolumeInfo "Volume",
       updated.Series,
	   updated.Periodical,
       updated.Author,
       updated.Year,
       updated.Edition,
       updated.Publisher,
       updated.City,
       updated.Pages,
       updated.PagesInFile,
       updated.Language,
       updated.Library,
       updated.Issue,
       updated.Identifier,
       updated.ISSN,
       updated.ASIN,
       updated.UDC,
       updated.LBC,
       updated.DDC,
       updated.LCC,
       updated.Doi "DOI",
       updated.Googlebookid,
       updated.OpenLibraryID,
       updated.Commentary,
       updated.DPI,
       updated.Color,
       updated.Cleaned,
       updated.Orientation,
       updated.Paginated,
       updated.Scanned,
       updated.Bookmarked,
       updated.Searchable,
       updated.Filesize,
       updated.Extension,
       updated.Generic,
       updated.Visible,
       updated.Locator,
       updated.Local,
       updated.TimeAdded,
       updated.TimeLastModified,
       updated.Coverurl "Cover",
       updated.Tags,
       updated.IdentifierWODash "ISBN",
       updated.MD5 "MD5",
       hashes.crc32 "CRC32",
       hashes.edonkey "edonkey",
       hashes.aich "AICH",
       hashes.sha1 "SHA1",
       hashes.tth "TTH",
       hashes.torrent "Torrent_Hash",
       hashes.btih "BTIH",
       hashes.sha256 "SHA256",
       description.toc "TOC",
       description.descr "Description",
       description.TimeLastModified "DescriptionTimeLastModified",
       topics_edited.topic_descr "Topic_English",
       topics_edited.topic_descr_rus "Topic_Russian",
       topics_edited.topic_id_hl, 
       ipfs_hashes.ipfs_blake2b "IPFS_Blake2b"
       FROM libgen.updated LEFT JOIN libgen.description ON libgen.updated.MD5=libgen.description.md5 LEFT JOIN libgen.ipfs_hashes ON libgen.updated.MD5=libgen.ipfs_hashes.md5 LEFT JOIN libgen.hashes ON libgen.updated.MD5=libgen.hashes.md5 LEFT JOIN libgen.topics_edited ON libgen.updated.Topic=libgen.topics_edited.topic_id ORDER BY libgen.updated.id LIMIT $counter,50000;
DROP TABLE IF EXISTS knowl;
ENDSQL
counter=$(( $counter + 50000 ))
done

# COMBINE ALL OF THE 100K JSON FILES
sudo cat knowl-libgen-science-master*.json >> ~/knowl/ingest/knowl-libgen-science-master_temp.json
#sudo rm -f knowl-libgen-science*.json

# DELETE LINES CONTAINING ONLY "[" or "]",
# AND DELETE LEADING WHITESPACE FROM ALL LINES
cd ~/knowl/ingest
sudo sed -i '/^\[/d;/^\]/d;s/^[ \t]*//' knowl-libgen-science-master_temp.json

#DELETE ILLEGAL AND CONTROL CHARACTERS
sudo sed -i $'s/[^[:print:]\t]//g' knowl-libgen-science-master_temp.json

#CORRECT UTF8 FORMATTING ERRORS
iconv -f utf-8 -t utf-8 -c knowl-libgen-science-master_temp.json > knowl-libgen-science-master-v1_1.json

sudo rm -f knowl-libgen-science-master_temp.json

# Next, you should call the index json script
# sudo sh ../bare-metal/index-science-master-json.sh
